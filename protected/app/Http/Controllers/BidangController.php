<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\Bidang;

class BidangController extends Controller
{
    /**
     * tambah bidang
     */
    function create(Request $request) {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		$data = [
				'title'    => 'Tambah Bidang',
				'menu'     => 'bidang',
				'sub_menu' => 'bidang tambah'
    		];
    		return view('content.bidang.create', $data);
    	}
    	else {
    		$save = Bidang::create($post);

    		return parent::redirect($save, 'Data bidang berhasil ditambahkan.');
    	}
    }

    /**
     * list
     */
    function index(Request $request) {
		$data = [
			'title'    => 'List Bidang',
			'menu'     => 'bidang',
			'sub_menu' => 'bidang list'
		];

		$data['bidang'] = Bidang::get()->toArray();

		return view('content.bidang.list', $data);
    }

    /**
     * delete
     */
    function delete(Request $request) {
		$post   = $request->except('_token');
		
		$delete = Bidang::where('id_bidang', $post['id_bidang'])->delete();

    	return parent::redirect($delete, 'Data bidang berhasil dihapus.');
    }

    /**
     * update
     */
    function update(Request $request, $id) {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		$data = [
    			'title'    => 'List Bidang',
    			'menu'     => 'bidang',
    			'sub_menu' => 'bidang list'
    		];

    		$bidang = Bidang::where('id_bidang', $id)->get()->toArray();

    		if (empty($bidang)) {
    			return back()->withErrors(['Data bidang tidak ditemukan.']);
    		}
    		else {
    			// ambil array ke 0
    			$data['bidang'] = $bidang[0];

    			return view('content.bidang.update', $data);
    		}
    	}
    	else {
    		$update = Bidang::where('id_bidang', $post['id_bidang'])->update($post);

    		return parent::redirect($update, 'Data bidang berhasil diperbarui.', 'bidang');
    	}
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Disposisi;
use App\Http\Models\Bidang;
use App\Http\Models\Jabatan;
use App\Http\Models\Surat;
use App\Http\Models\Koneksi;
use App\Http\Models\KoneksiDisposisi;

use Auth;

class DisposisiController extends Controller
{
	function search(Request $request) {
		$post = $request->except('_token');

		if (empty($post)) {
    		$data = [
				'title'    => 'Cari Surat',
				'menu'     => 'disposisi',
				'sub_menu' => 'disposisi tambah'
    		];

    		return view('content.disposisi.filter', $data);
		}
		else {
			
			$cari = Disposisi::leftjoin('surat', 'surat.id_surat', '=', 'disposisi.id_surat')->leftjoin('users', 'users.id_user', '=', 'disposisi.created_by')->where('kode', $post['kode'])->get()->toArray();

			if (empty($cari)) {
				return redirect('disposisi/tambah/'.$post['kode']);
			}
			else {
	    		$data = [
					'title'    => 'Detail Disposisi Surat',
					'menu'     => 'disposisi',
					'sub_menu' => 'disposisi tambah',
	    		];

	    		foreach ($cari as $key => $value) {
	    			$bidang = Koneksi::with(['bidang'])->where('id_surat', $value['id_surat'])->get()->toArray();

	    			if (empty($bidang)) {
	    				return redirect('disposisi/tambah/'.$post['kode']);
	    			}
	    			else {
	    				$cari[$key]['bidang'] = $bidang;
	    			}
	    		}

	    		$data['disposisi'] = $cari;

				return view('content.disposisi.detail', $data);
			}
		}
	}

	function create(Request $request, $kode) {

		$post = $request->except('_token');

		if (empty($post)) {
			$data = [
				'title'    => 'Cari Surat',
				'menu'     => 'disposisi',
				'sub_menu' => 'disposisi tambah'
			];

			$surat = Surat::where('kode', $kode)->get()->toArray();

			if (empty($surat)) {
				return back()->withErrors(['Data surat tidak ditemukan.']);
			}
			else {
				foreach ($surat as $key => $value) {
					$bidang = Koneksi::with(['bidang'])->where('id_surat', $value['id_surat'])->get()->toArray();

					if (empty($bidang)) {
						return redirect('disposisi/tambah/'.$post['kode']);
					}
					else {
						$surat[$key]['bidang'] = $bidang;
					}
				}

				$data['surat'] = $surat;

				// print_r($data); exit();
				return view('content.disposisi.create', $data);
			}
		}
		else {
			$kode = $post['kode'];

			unset($post['kode']);

			$post['created_by'] = Auth::user()->id_user;

			
			$save = Disposisi::create($post);

			if ($save) {
				// simpan notifikasi
				$saveNotif = parent::saveNotif($post['id_surat'], ['notif' => 'Disposisi untuk surat '.$kode, 'type' => 'disposisi']);
			}

			return parent::redirect($save, 'Data disposisi berhasil ditambahkan.', 'disposisi');
		}
	}

	function index(Request $request) {
		$data = [
			'title'    => 'List Disposisi',
			'menu'     => 'disposisi',
			'sub_menu' => 'disposisi list'
		];

		$cari = Disposisi::leftjoin('surat', 'surat.id_surat', '=', 'disposisi.id_surat')->leftjoin('users', 'users.id_user', '=', 'disposisi.created_by')->get()->toArray();

		if (!empty($cari)) {
			foreach ($cari as $key => $value) {
    			$bidang = Koneksi::with(['bidang'])->where('id_surat', $value['id_surat'])->get()->toArray();

    			$cari[$key]['bidang'] = $bidang;
    			

    			$user = User::where('id_user', $value['created_by'])->get()->toArray();

    			if (empty($user)) {
    				$cari[$key]['user'] = $user;
    			}
    			else {
    				$cari[$key]['user'] = $user[0];
    			}
    		}
		}

		$data['disposisi'] = $cari;

		// print_r($data); exit();

		return view('content.disposisi.list', $data);
	}

	function detail(Request $request, $kode) {
		$cari = Disposisi::leftjoin('surat', 'surat.id_surat', '=', 'disposisi.id_surat')->leftjoin('users', 'users.id_user', '=', 'disposisi.created_by')->where('kode', $kode)->get()->toArray();

		if (empty($cari)) {
			return redirect('disposisi/tambah/'.$kode);
		}
		else {
    		$data = [
				'title'    => 'Detail Disposisi Surat',
				'menu'     => 'disposisi',
				'sub_menu' => 'disposisi list',
    		];

    		foreach ($cari as $key => $value) {
    			$bidang = Koneksi::with(['bidang'])->where('id_surat', $value['id_surat'])->get()->toArray();

    			if (empty($bidang)) {
    				return redirect('disposisi/tambah/'.$post['kode']);
    			}
    			else {
    				$cari[$key]['bidang'] = $bidang;
    			}
    		}

    		$data['disposisi'] = $cari;

			return view('content.disposisi.detail', $data);
		}
		
	}

	function read(Request $request, $id) {
		parent::updateStatusRead($id, '1');

		return redirect('disposisi');
	}
}

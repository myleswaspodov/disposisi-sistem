<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Models\User;
use Auth;

class HomeController extends Controller
{
    function __construct() {
        $this->middleware('auth');
    }

    /**
     * home
     */
    function index(Request $request) {

    	$data['info'] = User::with(['jabatan', 'bidang'])->where('id_user', Auth::user()->id_user)->get()->toArray();
    	$data['menu'] = "home";
    	
    	return view('content.home', $data);
    }

}

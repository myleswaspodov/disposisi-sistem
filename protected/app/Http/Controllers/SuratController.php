<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Bidang;
use App\Http\Models\Jabatan;
use App\Http\Models\Surat;
use App\Http\Models\Koneksi;

use Auth;

class SuratController extends Controller
{
    /**
     * tambah surat
     */
    function create(Request $request) {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		$data = [
				'title'    => 'Tambah Surat',
				'menu'     => 'surat',
				'sub_menu' => 'surat tambah'
    		];

    		// ambil bidang
			$bidang         = Bidang::get()->toArray();

			if (empty($bidang)) {
				return redirect('bidang')->withErrors(['Mohon melengkapi data bidang.']);
			}
			else {
				$data['bidang'] = $bidang;
			}

    		return view('content.surat.create', $data);
    	}
    	else {

    		// print_r($post); exit();

    		// save pdf
    		$surat = $this->upload($request->file('scan'), $post['kode']);

    		if ($surat) {
    			$post['scan'] = $surat;

    			// print_r($post);

    			$saveSurat = $this->saveSurat($post);

    			if ($saveSurat) {
    				// save koneksi
    				$saveKoneksi = $this->saveKoneksi($saveSurat->id_surat, $post['id_bidang']);

    				return parent::redirect($saveKoneksi, 'Data surat berhasil ditambahkan.');
    			}
    			else {
    				return back()->withErrors(['Terjadi kesalahan. Silakan coba lagi.']);
    			}
    		}
    		else {
    			return back()->withErrors(['Upload file gagal. Silakan coba lagi.']);
    		}
    	}
    }

    /**
     * upload file
     */
    function upload($file, $nama) {
        $nama = str_replace('/', '_', $nama);
        
        $ext  = $file->getClientOriginalExtension();

        if ($file->move(base_path() . '/surat', $nama.'.'.$ext)) {
            
            return $nama.'.'.$ext;
        }
        else {
            return false;
        }
    }

    /**
     * save surat
     */
    function saveSurat($post=[]) {
    	$data = [
			'kode'      => $post['kode'],
			'perihal'   => $post['perihal'],
			'sinopsis'  => $post['sinopsis'],
			'scan'      => 'protected/surat/'.$post['scan'],
			'tgl_surat' => date('Y-m-d', strtotime($post['tgl_surat']))
    	];
    	
    	$save = Surat::create($data);

    	return $save;
    }

    /**
     * untuk bidang tertentu
     */
    function saveKoneksi($id_surat, $id_bidang=[]) {
    	$data = [];
    	
    	foreach ($id_bidang as $value) {
    		$temp = [
    			'id_surat' => $id_surat,
    			'id_bidang' => $value,
    			'created_at' => date('Y-m-d H:i:s'),
    			'updated_at' => date('Y-m-d H:i:s')
    		];

    		array_push($data, $temp);
    	}

    	$save = Koneksi::insert($data);

    	return $save;
    }

    /**
     * list
     */
    function index(Request $request) {
        $data = [
            'title'    => 'List Surat',
            'menu'     => 'surat',
            'sub_menu' => 'surat list'
        ];

        // user
        $user = Auth::user();

        if ($user->is_admin == 1) {
            $surat = Surat::get()->toArray();

            if (!empty($surat)) {
                foreach ($surat as $key => $value) {
                    // bidang mana yg terkait
                    $bidang = Koneksi::join('bidang', 'bidang.id_bidang', '=', 'koneksi.id_bidang')->where('id_surat', $value['id_surat'])->get()->toArray();

                    $surat[$key]['bidang'] = $bidang;
                }
            }
            
            $data['surat'] = $surat;
            
        }
        else {
            $data['surat'] = array_pluck(Koneksi::with(['surat'])->where('id_bidang', Auth::user()->id_bidang)->get()->toArray(), "surat");
            // $data['surat'] = array_pluck(Koneksi::with(['surat'])->get()->toArray(), "surat");
        }

        // print_r($data); exit();

        return view('content.surat.list', $data);
    }

}

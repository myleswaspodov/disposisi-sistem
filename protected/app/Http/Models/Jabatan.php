<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_jabatan
 * @property string $jabatan
 * @property string $created_at
 * @property string $updated_at
 * @property User[] $users
 */
class Jabatan extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'jabatan';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_jabatan';

    /**
     * @var array
     */
    protected $fillable = ['jabatan', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Http\Models\User', 'id_jabatan', 'id_jabatan');
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_notif
 * @property int $id_user
 * @property string $notif
 * @property string $type
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class Notif extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notif';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_notif';

    /**
     * @var array
     */
    protected $fillable = ['id_bidang', 'notif', 'type', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bidang()
    {
        return $this->belongsTo('App\Http\Models\Bidang', 'id_bidang', 'id_bidang');
    }
}

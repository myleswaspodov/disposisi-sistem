<?php

namespace App\Http\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id_user
 * @property int $id_bidang
 * @property int $id_jabatan
 * @property string $name
 * @property string $email
 * @property string $npk
 * @property string $password
 * @property string $id_admin
 * @property string $remember_token
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property Jabatan $jabatan
 * @property Bidang $bidang
 * @property Disposisi[] $disposisis
 * @property Notif[] $notifs
 */
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_user';

    protected $hidden = ['password'];

    /**
     * @var array
     */
    protected $fillable = ['id_bidang', 'id_jabatan', 'name', 'phone', 'email', 'npk', 'password', 'is_admin', 'remember_token', 'deleted_at', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jabatan()
    {
        return $this->belongsTo('App\Http\Models\Jabatan', 'id_jabatan', 'id_jabatan');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bidang()
    {
        return $this->belongsTo('App\Http\Models\Bidang', 'id_bidang', 'id_bidang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function disposisis()
    {
        return $this->hasMany('App\Http\Models\Disposisi', 'created_by', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifs()
    {
        return $this->hasMany('App\Http\Models\Notif', 'id_user', 'id_user');
    }
}

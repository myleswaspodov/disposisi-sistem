<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKoneksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koneksi', function (Blueprint $table) {
            $table->increments('id_koneksi');
            // relasi surat
            $table->unsignedInteger('id_surat');
            $table->foreign('id_surat')->references('id_surat')->on('surat')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            // relasi bidang
            $table->unsignedInteger('id_bidang');
            $table->foreign('id_bidang')->references('id_bidang')->on('bidang')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koneksi');
    }
}

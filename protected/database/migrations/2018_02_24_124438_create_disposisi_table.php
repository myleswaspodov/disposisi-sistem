<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisposisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('disposisi', function (Blueprint $table) {
            $table->increments('id_disposisi');
            $table->text('disposisi');
            // user
            $table->unsignedInteger('created_by');
            $table->foreign('created_by')->references('id_user')->on('users')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            // koneksi
            $table->unsignedInteger('id_koneksi');
            $table->foreign('id_koneksi')->references('id_koneksi')->on('koneksi')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('disposisi');
    }
}

<?php

use Illuminate\Database\Seeder;

class BidangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('bidang')->delete();
        
        \DB::table('bidang')->insert([
    		[
				'bidang'     => 'Pemasaran', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'bidang'     => 'Pelayanan', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'bidang'     => 'Umum & SDM', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'bidang'     => 'Keuangan', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'bidang'     => 'Wasrik', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'bidang'     => 'KCP', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
        ]);
    }
}

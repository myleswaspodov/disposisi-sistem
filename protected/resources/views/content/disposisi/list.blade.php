@extends('template.body')

@section('style')
	
	<link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css">') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>User</li>
	    	<li class="active">List</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <div class="box-body">
	      <table id="table01" class="table table-bordered table-striped">
	        <thead>
	        <tr>
	          	<th>No Surat</th>
	          	<th>Tgl Surat</th>
	          	<th>Perihal</th>
	          	<th>Disposisi</th>
	          	<th>Dibuat oleh</th>
	          	<th>Bidang</th>
	          	<th>Action</th>
	        </tr>
	        </thead>
	        <tbody>
	        @if (!empty($disposisi))
		        @foreach ($disposisi as $key=>$val)
		        	<tr>
			          	<td>{{ $val['kode'] }}</td>
			          	<td>{{ date('d F Y', strtotime($val['tgl_surat'])) }}</td>
			          	<td>{{ $val['perihal'] }}</td>
			          	<td>{{ $val['disposisi'] }}</td>
			          	<td>{{ $val['name'] }}</td>
          	          	@if (empty($val['bidang']))
          	          	<td> - </td>
          	          	@else
          	          		<td> 
          	          		@foreach ($val['bidang'] as $u) 
          						<li> {{ $u['bidang']['bidang'] }} </li>
          	          		@endforeach
          	          		</td>
          	          	@endif
			          	<td>
			          		<a href="{{ url('disposisi/detail', $val['kode']) }}" class="btn btn-primary"><i class="fa fa-search"></i> Detail </a>
			          	</td>
			        </tr>
			    @endforeach
		    @endif
	        </tbody>
	      </table>
	    </div>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script src="{{ url('js/bootstrap-confirmation.js') }}"></script>
<script>
  $(function () {
    $('#table01').DataTable();
    
  });
</script>
<script type="text/javascript">
	$('[data-toggle=confirmation-popout]').confirmation({
	    rootSelector: '[data-toggle=confirmation-popout]',
	    container: 'body'
	});
</script>
<script type="text/javascript">
	$('#table01').on('click', '.delete', function() {
		$('#delete-'+$(this).data('id')).submit();
	});

	$('#table01').on('click', '.admin', function() {
		$('#admin-'+$(this).data('id')).submit();
	});
</script>

@endsection 
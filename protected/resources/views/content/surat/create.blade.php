@extends('template.body')

@section('style')
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>Surat</li>
	    	<li class="active">Tambah</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST" enctype="multipart/form-data">
	      	<div class="box-body">
		        <div class="form-group">
		          <label for="inputEmail3" class="col-md-2 control-label">Kode </label>

		          <div class="col-md-10">
		            <input type="text" class="form-control" name="kode" placeholder="Kode Surat" required maxlength="100" value="{{ old('kode') }}">
		          </div>
		        </div>
		       
		        <div class="form-group">
		          <label for="inputEmail3" class="col-md-2 control-label">Tanggal Surat </label>

		          <div class="col-md-10">
		            <div class="input-group">
	                  <div class="input-group-addon">
	                    <i class="fa fa-calendar"></i>
	                  </div>
	                  <input type="date" class="form-control" name="tgl_surat">
	                </div>
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="inputEmail3" class="col-md-2 control-label">Perihal </label>

		          <div class="col-md-10">
		            <input type="text" class="form-control" name="perihal" placeholder="Perihal" required value="{{ old('perihal') }}">
		          </div>
		        </div>
		        <div class="form-group">
		          <label for="inputEmail3" class="col-md-2 control-label">Sinopsis </label>

		          <div class="col-md-10">
		            <textarea class="form-control" name="sinopsis" placeholder="Sinopsis"></textarea>
		          </div>
		        </div>
                <div class="form-group">
                  	<label class="col-md-2 control-label">Ditujukan Pada Bidang</label>
                  	<div class="col-md-10">
        	          	<select class="form-control " multiple="multiple" data-placeholder="Pilih Bidang" name="id_bidang[]" required>
                  		@foreach($bidang as $val)
        					<option value="{{ $val['id_bidang'] }}"> {{ $val['bidang'] }}</option>
        				@endforeach
        				</select>
                  </div>
                </div>
		        <div class="form-group">
		        	<label for="inputEmail3" class="col-md-2 control-label">Upload Berkas </label>
                  	<div class="col-md-10">
                  		<input type="file" accept="application/pdf" required name="scan">
                  		<p class="help-block">File (.pdf)</p>
                  	</div>
                </div>
	      	</div>
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
				{{ csrf_field() }}	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
	
	
	<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.select2').select2();
		});
	</script>
@endsection 
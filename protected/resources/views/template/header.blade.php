<header class="main-header">
  <!-- Logo -->
  <a href="{{ url('home') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>S</b>D</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>Sistem</b> Disposisi</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Notifications: style can be found in dropdown.less -->
        <li class="dropdown notifications-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            @if (count(newqueryNotification()) > 0)
            <span class="label label-warning">{{ count(newqueryNotification()) }}</span>
            @endif
          </a>
          <ul class="dropdown-menu">
            <li class="header">{{ count(queryNotification()) }} notifications</li>
            <li>
              @php
                
              @endphp
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                  {{ notification() }}
              </ul>
            </li>
            <!-- <li class="footer"><a href="#">View all</a></li> -->
          </ul>
        </li>
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="javascript();" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ url('dist/img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ Auth::user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="#" class="btn btn-warning">Ganti Password</a>
              </div>
              <div class="pull-right">
                <a href="{{ route('logout') }}" class="btn btn-danger" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Sign out</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </div>
            </li>
          </ul>
        </li>
       
      </ul>
    </div>
  </nav>
</header>
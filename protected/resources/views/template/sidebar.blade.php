<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="@if (isset($menu) && $menu == 'home') active @endif">
          <a href="{{ url('home') }}">
            <i class="fa fa-home"></i> <span>Home</span>
          </a>
        </li>
        <!-- --------------------------------------------------------------------- -->
        <!-- data master -->

        <li class="header">MASTER</li>
        <!-- bidang -->
        <li class="treeview @if (isset($menu) && $menu == 'bidang') active @endif">
          <a href="javascript();">
            <i class="fa fa-laptop"></i>
            <span>Bidang</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'bidang tambah') active @endif"><a href="{{ url('bidang/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'bidang list') active @endif"><a href="{{ url('bidang') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        <!-- jabatan -->
        <li class="treeview @if (isset($menu) && $menu == 'jabatan') active @endif">
          <a href="javascript();">
            <i class="fa fa-signal"></i>
            <span>Jabatan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('jabatan/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li><a href="{{ url('jabatan') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        <!-- admin & user -->
        <li class="treeview @if (isset($menu) && $menu == 'user') active @endif">
          <a href="javascript();">
            <i class="fa fa-user"></i>
            <span>User & Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'user tambah') active @endif"> <a href="{{ url('user/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'user list') active @endif"><a href="{{ url('user') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        
        <li class="header">SURAT</li>
        <!-- surat -->
        <li class="treeview @if (isset($menu) && $menu == 'surat') active @endif">
          <a href="javascript();">
            <i class="fa fa-envelope"></i>
            <span>Surat</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'surat tambah') active @endif"><a href="{{ url('surat/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'surat list') active @endif"><a href="{{ url('surat') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        <!-- disposisi -->
        <li class="treeview  @if (isset($menu) && $menu == 'disposisi') active @endif">
          <a href="javascript();">
            <i class="fa fa-sticky-note-o"></i>
            <span>Disposisi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="@if (isset($sub_menu) && $sub_menu == 'disposisi tambah') active @endif"><a href="{{ url('disposisi/tambah') }}"><i class="fa fa-circle"></i> Tambah</a></li>
            <li class="@if (isset($sub_menu) && $sub_menu == 'disposisi list') active @endif"><a href="{{ url('disposisi') }}"><i class="fa fa-circle"></i> List</a></li>
          </ul>
        </li>
        
                
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
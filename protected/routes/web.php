<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Auth::routes();

/**
 * routes home
 */
Route::get('/home', 'HomeController@index');


Route::group(['middleware' => ['web', 'auth']], function()
{
	/**
	 * bidang
	 */    
    Route::group(['prefix' => 'bidang'], function()
    {
        Route::get('/', 'BidangController@index');
        Route::any('tambah', 'BidangController@create');
        Route::any('update/{id}', 'BidangController@update');
        Route::post('hapus', 'BidangController@delete');
    });

    /**
     * user
     */
    Route::group(['prefix' => 'user'], function()
    {
        Route::get('/', 'UserController@index');
        Route::any('tambah', 'UserController@create');
        Route::any('update/{id}', 'UserController@update');
        Route::post('hapus', 'UserController@delete');
    });

    /**
     * surat
     */
    Route::group(['prefix' => 'surat'], function()
    {
        Route::get('/', 'SuratController@index');
        Route::any('tambah', 'SuratController@create');
        Route::any('update/{id}', 'SuratController@update');
        Route::post('hapus', 'SuratController@delete');
    });

    /**
     * surat
     */
    Route::group(['prefix' => 'disposisi'], function()
    {
        Route::any('/', 'DisposisiController@index');
        Route::any('tambah', 'DisposisiController@search');
        Route::any('tambah/{id}', 'DisposisiController@create');
        Route::any('update/{id}', 'DisposisiController@update');
        Route::any('detail/{id}', 'DisposisiController@detail');
        Route::post('hapus', 'DisposisiController@delete');
        Route::get('read/{id}', 'DisposisiController@read');
    });
});



